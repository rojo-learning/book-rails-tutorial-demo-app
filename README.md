# Ruby on Rails Tutorial: Microposts Demo #

This is a demo application for the [*Ruby on Rails Tutorial*](http://railstutorial.org/) by [Michael Hartl](http://michaelhartl.com/).

A quite basic twitter like app to show the generation of resources using scaffolding, and basic use of validations and model relations.